﻿using System;

namespace ZAD1
{
    class Program
    {
        static void Main(string[] args)
        {
            Notebook notebook = new Notebook();
            
            Note pi = new Note("Number PI", "3.14159265359");
            Note FBI = new Note("FBI", "Federal Bureau of Investigation");
            Note CSI = new Note("CSI", "Crime Scene Investigation");                              
            notebook.AddNote(pi);
            notebook.AddNote(FBI);
            notebook.AddNote(CSI);

            IAbstractIterator iterator = notebook.GetIterator();

            while(iterator.IsDone == false)
            {
                iterator.Current.Show();
                iterator.Next();
            }
            Note LBJ = new Note("LBJ", "LeBron James");
            Console.WriteLine("\n\n\n");
            iterator.First().Show();
            notebook.RemoveNote(pi);
            iterator.First().Show();
            Console.WriteLine("\n\n\n");
            notebook.Clear();
            notebook.AddNote(LBJ);
            iterator.First().Show();
        }
    }
}

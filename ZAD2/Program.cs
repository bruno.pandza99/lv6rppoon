﻿using System;

namespace ZAD2
{
    class Program
    {
        static void Main(string[] args)
        {
            Box box = new Box();
            Product OledTV = new Product("4K OLED TV", 2000);
            Product Graphics = new Product("NVIDIA Geforce 2080TI", 1500);
            Product mouse = new Product("Finalmouse air58", 400);
            box.AddProduct(OledTV);
            box.AddProduct(Graphics);
            box.AddProduct(mouse);

            IAbstractIterator iterator = box.GetIterator();
            while(iterator.IsDone == false)
            {
                Console.WriteLine(iterator.Current.ToString());
                iterator.Next();
            }
            Console.WriteLine("\n\n\n");
            Console.WriteLine(iterator.First().ToString());
            box.RemoveProduct(OledTV);
            Console.WriteLine(iterator.First().ToString());

        }
    }
}
